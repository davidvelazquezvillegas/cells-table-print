/* global moment */
(function() {

  'use strict';

  Polymer({

    is: 'cells-table-print',

    behaviors: [
      window.CellsBehaviors.i18nBehavior
    ],
    properties: {
      cols: {
        type: Array
      },
      items: {
        type: Array
      },
      itemFormatDate: {
        type: String,
        value: 'DD MMM'
      },
      formatDate: {
        type: String,
        value: 'DD/MM/YYYY'
      }
    },
    checkedType: function(fieldType, type) {
      return (fieldType && fieldType.toLowerCase() === type);
    },
    _parseDate: function(date) {
      var locale = (I18nMsg.lang) ? I18nMsg.lang.split('-')[0] : 'en';
      var res = moment(date, this.formatDate).locale(locale);
      return res.format(this.itemFormatDate);
    },
    _parseIcon: function(icon) {
      var iconDesc = '';
      if (icon && icon.family && icon.num) {
        iconDesc = icon.family + ':' + icon.num;
      }
      return iconDesc;
    },
    /*
     * Method computed, checked if has family and icon in value of item type icon
     * @private
    */
    checkedIcon: function(item) {
      return (item && item.family && item.num);
    },
    copy: function(){
      var table = Polymer.dom(this.root).querySelector('#tableData');
      console.log(table)
      var range = document.createRange();
      range.selectNode(table);
      window.getSelection().addRange(range);
      
        try {
          // intentar copiar el contenido seleccionado
          var resul = document.execCommand('copy');
          console.log("RESULTADO",resul)
          console.log(resul ? 'Email copiado' : 'No se pudo copiar');
        } catch(err) {
          console.log('ERROR al intentar copiar',err);
        }
      
        // eliminar el texto seleccionado
        window.getSelection().removeAllRanges();
        // cuando los navegadores lo soporten, habría
        // que utilizar: removeRange(range)
       
    }
  });

}());
